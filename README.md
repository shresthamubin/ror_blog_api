# Installation #

This blog api uses rails-api & mysql database as backend. Refer following blogs to install rails and mysql.

* http://ryanbigg.com/2014/10/ubuntu-ruby-ruby-install-chruby-and-you//
* https://www.digitalocean.com/community/tutorials/how-to-use-mysql-with-your-ruby-on-rails-application-on-ubuntu-14-04

After complete installation of mysql and rails-api. Follow below instruction:
RUN following in exact order

```
#!ruby

1. rake db:create
2. rake db:migrate
3. rails-api server
```

The above command will create the database and necessary tables. It then starts the rails server exposing the api.

# POSTS #

**Creating a blog post.**

```
#!ruby

curl -d "post[title]=First Post On Ruby On Rails" -d "post[text]=Creating blog api using Ruby On Rails was fun" http://localhost:3000/posts
```

**OUTPUT**

```
#!ruby

{
  "id": 1,
  "title": "First Post On Ruby On Rails",
  "text": "Creating blog api using Ruby On Rails was fun",
  "created_at": "2016-02-13T19:48:48.181Z",
  "updated_at": "2016-02-13T19:48:48.181Z"
}

```

**Reading all blog post**

```
#!ruby

curl -X GET http://localhost:3000/posts
```

**OUTPUT**

```
#!ruby

{
  "id": 1,
  "title": "First Post On Ruby On Rails",
  "text": "Creating blog api using Ruby On Rails was fun",
  "created_at": "2016-02-13T19:48:48.181Z",
  "updated_at": "2016-02-13T19:48:48.181Z"
}

```

**Reading only one blog post**


```
#!ruby

curl -X GET http://localhost:3000/posts/[:id]
```


For e.g:


```
#!ruby

curl -X GET http://localhost:3000/posts/1
```

**OUTPUT**

```
#!ruby

{
  "id": 1,
  "title": "First Post On Ruby On Rails",
  "text": "Creating blog api using Ruby On Rails was fun",
  "created_at": "2016-02-13T19:48:48.181Z",
  "updated_at": "2016-02-13T19:48:48.181Z"
}
```

**Updating a blog post**

```
#!ruby

curl --request PATCH -d "post[title]=Ruby Rock" -d "post[text]=Creating blog api using Ruby On Rails was fun" http://localhost:3000/posts/1
```

**OUTPUT**

```
#!ruby

{
  "id": 1,
  "title": "Ruby Rock",
  "text": "Creating blog api using Ruby On Rails was fun",
  "created_at": "2016-02-13T19:48:48.000Z",
  "updated_at": "2016-02-13T19:52:52.896Z"
}
```

**Deleting a blog post**

```
#!ruby

curl --request DELETE http://localhost:3000/posts/1
```

**OUTPUT**

Deleted Post with Id: 1

# COMMENTS #

**Creating a comment for a post**

URL: POST   http://localhost:3000/posts/:post_id/comments


```
#!ruby

curl -d "comment[commenter]=Mubin Shrestha" -d "comment[body]=Ruby on rails is amazing to learn" http://localhost:3000/posts/1/comments
```


**OUTPUT**

```
#!ruby

{
  "id": 1,
  "commenter": "Mubin Shrestha",
  "body": "Ruby on rails is amazing to learn",
  "post_id": 1,
  "created_at": "2016-02-13T18:41:20.795Z",
  "updated_at": "2016-02-13T18:41:20.795Z"
}
```

**Reading a particular comment for a particular post**

URL: GET    http://localhost:3000/posts/:post_id/comments/:id

```
#!ruby

curl -X GET http://localhost:3000/posts/1/comments/1
```

**OUTPUT**

```
#!ruby

{
  "id": 1,
  "commenter": "Mubin Shrestha",
  "body": "Ruby on rails is amazing to learn",
  "post_id": 1,
  "created_at": "2016-02-13T18:41:20.795Z",
  "updated_at": "2016-02-13T18:41:20.795Z"
}
```

**Updating a comment**

URL: PATCH  /posts/:post_id/comments/:id

```
#!ruby

curl --request PATCH -d "comment[commenter]=Ruby Maharjan" -d "post[body]=Updated body" http://localhost:3000/posts/1/comments/1
```

**OUTPUT**

```
#!ruby

{
  "id": 1,
  "commenter": "Ruby Maharjan",
  "body": "Ruby on rails is amazing to learn",
  "post_id": 1,
  "created_at": "2016-02-13T18:41:20.000Z",
  "updated_at": "2016-02-13T18:47:02.015Z"
}
```

**Deleting a comment**

URL: DELETE http://localhost:3000/posts/:post_id/comments/:id

```
#!ruby

curl --request DELETE http://localhost:3000/posts/1/comments/1
```

**OUTPUT**

```
#!ruby

{"message":"Deleted Comment with Id: 1 Of Post Id: 1"}
```