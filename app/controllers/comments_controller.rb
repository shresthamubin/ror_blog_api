class CommentsController < ApplicationController
  # GET /posts/:post_id/comments
  def index
    #1st you retrieve the post thanks to params[:post_id]
    @post = Post.find(params[:post_id])
    #2nd you get all the comments of this post
    @comments = @post.comments
    render json:  @comments, status: :ok
  end

  # GET /posts/:post_id/comments/:id
  def show
    #1st you retrieve the post thanks to params[:post_id]
    @post = Post.find(params[:post_id])
    #2nd you retrieve the comment thanks to params[:id]
    @comment = @post.comments.find(params[:id])

    render json:  @comment, status: :ok
  end

  # GET /posts/:post_id/comments/new
  def new
    #1st you retrieve the post thanks to params[:post_id]
    @post = Post.find(params[:post_id])
    #2nd you build a new one
    @comment = @post.comments.build
    render json:  @comment, status: :ok
  end

  # GET /posts/:post_id/comments/:id/edit
  def edit
    #1st you retrieve the post thanks to params[:post_id]
    @post = Post.find(params[:post_id])
    #2nd you retrieve the comment thanks to params[:id]
    @comment = @post.comments.find(params[:id])
  end

  # POST /posts/:post_id/comments
  def create
    #1st you retrieve the post thanks to params[:post_id]
    @post = Post.find(params[:post_id])
    #2nd you create the comment with arguments in params[:comment]
    @comment = @post.comments.create(comment_params)
    if @comment.save
      render json:  @comment, status: 201
    else
      render json:  @comment, status: 422
    end
  end

  # PUT /posts/:post_id/comments/:id
  def update
    #1st you retrieve the post thanks to params[:post_id]
    @post = Post.find(params[:post_id])
    #2nd you retrieve the comment thanks to params[:id]
    @comment = @post.comments.find(params[:id])

    if @comment.update_attributes(comment_params)
      render json:  @comment, status: 201
    else
      render json:  @comment, status: 422
    end
  end

  # DELETE /posts/:post_id/comments/1
  def destroy
    #1st you retrieve the post thanks to params[:post_id]
    @post = Post.find(params[:post_id])
    #2nd you retrieve the comment thanks to params[:id]
    @comment = @post.comments.find(params[:id])

    @comment.destroy
    render json: {
      message: 'Deleted Comment with Id: ' + params[:id] + ' Of Post Id: ' + params[:post_id]}.to_json, status: 202
  end

  private

  def comment_params
    params.require(:comment).permit(:commenter, :body)
  end
end
