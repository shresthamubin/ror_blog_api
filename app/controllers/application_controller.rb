class ApplicationController < ActionController::API
  rescue_from Exception, with: :show_errors

  protected

  def show_errors(exception)
    render json: {
      message: exception.message
      }.to_json, status: 404
  end
end