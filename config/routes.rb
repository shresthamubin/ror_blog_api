Rails.application.routes.draw do
  resources :posts, only: [:index, :show, :create, :update, :destroy] do
    resources :comments, only: [:index, :show, :create, :update, :destroy]
  end
end
